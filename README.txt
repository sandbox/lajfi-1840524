CONTENTS OF THIS FILE
---------------------
* Introduction
* Installation
* Configuration

INTRODUCTION
------------
Current Maintainer: Lajfi (support@halemail.eu)

HaLe Mail is an email marketing platform using state of the art technology which permits you to send emails to your different email databases. 
This 100% online tool combines a high level of performance and penetration with an enormous ease of use. Thanks to its advanced statistics, you will 
know everything about your emailing campaign, which allows you to further develop and finetune your emailing strategies no matter where you are located in the world!

The HaLe Mail API module is needed to allow you to take advantage of the API made available for HaLe Mail.

INSTALLATION
------------
1. Copy the entire webform directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

CONFIGURATION
-------------
In order to be able to use the API, you need an API key. You will receive one after you have succesfully registered for HaLe Mail on www.halemail.eu.
You need to enter this API key on the following page:
=> admin/config/services/hm-api